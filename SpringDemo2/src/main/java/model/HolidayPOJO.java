package model;

public class HolidayPOJO {
private String Hdate,Hname;

public HolidayPOJO(String hdate, String hname) {
	super();
	Hdate = hdate;
	Hname = hname;
}
public String getHdate() {
	return Hdate;
}
public String getHname() {
	return Hname;
}
public void setHdate(String hdate) {
	Hdate = hdate;
}
public void setHname(String hname) {
	Hname = hname;
}
@Override
public String toString() {
	// TODO Auto-generated method stub
	return "HOLIDAY DATE IS:"+Hdate+ "and name is:"+Hname;
}
}
