package model;

public class Employee {
private int age;
private String name;
private String city;
public Employee(int age, String name) {
	super();
	this.age = age;
	this.name = name;
}
public Employee(String city,int age) {
	super();
	this.age = age;
	this.city = city;
}
public Employee(String name, String city) {
	super();
	this.name = name;
	this.city = city;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Employee name is "+name+" and age is "+age;
	} 
}
