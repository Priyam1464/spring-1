package p1;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.HolidayPOJO;

public class Demo2 {
public static void main(String[] args) {
	ApplicationContext context=new AnnotationConfigApplicationContext(JavaContainer.class);
	Hello ob;
	ob=context.getBean(Hello.class);
	ob.display();
	model.Employee ob2;
	ob2=(model.Employee)context.getBean(model.Employee.class);
	System.out.println(ob2.toString());
	model.ListPOJO listPOJO;
	listPOJO=(model.ListPOJO)context.getBean(model.ListPOJO.class);
	System.out.println(listPOJO.toString());
}
}
