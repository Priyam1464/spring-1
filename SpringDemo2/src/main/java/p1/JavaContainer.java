package p1;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import model.Employee;
import model.HolidayPOJO;
import model.ListPOJO;

@Configuration
public class JavaContainer {
@Bean
@Scope("singleton")
public Hello get1() {
return new Hello();//doesn't return the same object more than once.Stores it in memory
}
@Bean
public model.Employee get2()
{
model.Employee ob=new model.Employee(12, "Sudarshan");
return ob;
}
@Bean
model.ListPOJO get3()
{
model.ListPOJO ob;
ob=new model.ListPOJO();
ob.getList()
.add(new HolidayPOJO("12.10.19","Independence Day"));
ob.getList()
.add(new HolidayPOJO("13.11.19","World Cup Day"));
return ob;
}
}
