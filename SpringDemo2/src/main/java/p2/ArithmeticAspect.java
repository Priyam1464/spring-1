package p2;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.core.annotation.Order;

@Aspect
@Order(0) //which aspect to run is weaving or order
public class ArithmeticAspect {
	
	
    @Before("execution( * * *.*(double,double))")//arguments are join point when method is to be executed is point cut
	public void check1(JoinPoint jpoint)
	{
	for(Object x:jpoint.getArgs())	
	{
		
	double v=(Double)x;
	if(v<0)
	{
		throw new IllegalArgumentException("Number cannot be negative");
		
	}
	}
	}

}
