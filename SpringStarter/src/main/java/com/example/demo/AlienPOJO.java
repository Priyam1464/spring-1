package com.example.demo;

import org.springframework.stereotype.Component;

@Component
public class AlienPOJO {
private int id;
private String name,tech;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public String getTech() {
	return tech;
}
public void setTech(String tech) {
	this.tech = tech;
}
@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Alien name is "+name+",id is "+id+" and tech is "+tech;
	}
}
