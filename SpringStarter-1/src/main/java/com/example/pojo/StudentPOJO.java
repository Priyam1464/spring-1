package com.example.pojo;

public class StudentPOJO {
private String name,city;
private int age;

public StudentPOJO(String name, int age, String city) {
	super();
	this.name = name;
	this.age = age;
	this.city = city;
}
public String getName() {
	return name;
}

public void setName(String name) {
	this.name = name;
}

public int getAge() {
	return age;
}

public void setAge(int age) {
	this.age = age;
}

public String getCity() {
	return city;
}

public void setCity(String city) {
	this.city = city;
}

}
