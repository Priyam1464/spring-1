package com.example.demo;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
@RequestMapping("/hello")
public String display()
{
return "Welcome to Microservices";	
}
@RequestMapping("/pojo")
public com.example.pojo.StudentPOJO displayPOJO()
{
	com.example.pojo.StudentPOJO studentPOJO=new com.example.pojo.StudentPOJO("priyam", 21, "Chandigarh");
	return studentPOJO;
	
}
}
