package p1;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import model.HolidayPOJO;
import model.ListPOJO;

public class Demo1 {
public static void main(String[] args) {
ApplicationContext context=new ClassPathXmlApplicationContext("bean.xml");//This is object of factory class which provides container	
Hello ob;
ob=(Hello)context.getBean("hello");
ob.display();
model.HolidayPOJO ob2;
ob2=(model.HolidayPOJO)context.getBean("h1");
System.out.println(ob2.toString());
ListPOJO listPOJO = new ListPOJO();
listPOJO.getList().add((model.HolidayPOJO) context.getBean("h1"));
listPOJO.getList().add((model.HolidayPOJO) context.getBean("h2"));
listPOJO.getList().add((model.HolidayPOJO) context.getBean("h3"));
System.out.println(listPOJO.toString());
model.Employee employee=(model.Employee)context.getBean("employee");
System.out.println(employee.toString());
}
}
